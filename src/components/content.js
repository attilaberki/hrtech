import React, { Component } from 'react';
import { connect } from 'react-redux';

import Tabs from './content/tabs';
import PriceTabs from './content/price-tabs';
import Tickets from './content/tickets';
import Partners from './content/partners';

class Content extends Component {

  render() {
    return (
      <div className="Content">
        <Tabs />
        <PriceTabs />
        <Tickets />
        <Partners />
      </div>
    );
  }
}


function mapStateToProps({ data }){
  return { data: data[0] };
}

export default connect(mapStateToProps)(Content);
