import React, { Component } from 'react';

class Footer extends Component {

  render() {
    return (
      <div className="Footer">
        <a href="/" className="foot-logo"><img src="assets/vector/hr-tech-world-logo.svg"/></a>

        <nav className="foot-links">
          <ul>
            <li><a href="/">Hrn</a></li>
            <li><a href="/about">About</a></li>
            <li><a href="/team">Team</a></li>
            <li><a href="/jobs">Jobs</a></li>
            <li><a href="/contact">Contact</a></li>
          </ul>
        </nav>

        <nav className="foot-social">
          <ul>
            <li><a href="https://twitter.com/hrtechworld" target="_blank"><i className="fa fa-twitter"></i></a></li>
            <li><a href="https://www.facebook.com/humanresourcesnetwork/" target="_blank"><i className="fa fa-facebook"></i></a></li>
            <li><a href="https://www.linkedin.com/company/hrn-europe---pan-european-hr-network?trk=tyah&trkInfo=tarId%3A1420453121204%2Ctas%3Apan+europe%2Cidx%3A2-1-6" target="_blank"><i className="fa fa-linkedin"></i></a></li>
            <li><a href="https://www.instagram.com/hrtechconf/" target="_blank"><i className="fa fa-instagram"></i></a></li>
            <li><a href="https://www.youtube.com/user/hrneurope/" target="_blank"><i className="fa fa-youtube-play"></i></a></li>
          </ul>
        </nav>

        <nav className="foot-etc">
          <span>HRN</span>
         |<span><a href="/terms">Terms & Conditions</a></span>
         |<span><a href="/cookie-policy">Cookie Policy</a></span>
         |<span>Copyright © 2017 HRN.</span>
         |<span>All Rights Reserved.</span>
        </nav>
      </div>
    );
  }
}

export default Footer;
