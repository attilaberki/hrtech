import React, { Component } from 'react';

class Header extends Component {

  render() {
    return (
      <div className="Header">
        <a href="/"><img src="assets/vector/menu-logo-2.svg"/></a>
      </div>
    );
  }
}


export default Header;
