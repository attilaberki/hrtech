import React, { Component } from 'react';
import { connect } from 'react-redux';

import { changePrice } from '../../actions/index';
import PriceTab from "./price-tab";

class PriceTabs extends Component {

  state = { phase: 'first' }

  componentWillReceiveProps(nextProps) {
    this.setState({ phase: 'rendered' });
  }

  tabClick(name) {
    this.props.changePrice(name);
  }

  renderPriceinfo() {
    const selected = this.props.data.pages.filter((page) => {
      return page.name === this.props.data.settings.page;
    });
    return <span>{selected[0].lead}</span>;
  }

  renderPer(before, after) {
    if(this.props.data.settings.price === before || this.props.data.settings.price === after) {
      return <li className="per active">/</li>;
    }
    return <li className="per">/</li>;
  }

  render() {
    const { prices } = this.props.data.labels;
    const infokey = `info-${this.props.data.settings.page}`;
    const classname = `PriceTabs ${this.state.phase}`;
    return (
      <div className={classname}>
        <ul>
          <PriceTab
            current={this.props.data.settings.price}
            click={this.tabClick.bind(this)}
            title={prices.early}
            name="early"
            soldout={this.props.data.labels.soldout}
          />
          {this.renderPer('early', 'summer')}
          <PriceTab
            current={this.props.data.settings.price}
            click={this.tabClick.bind(this)}
            title={prices.summer}
            name="summer"
            until={this.props.data.labels.until}
            date={this.props.data.labels.summerend}
          />
          {this.renderPer('summer', 'regular')}
          <PriceTab
            current={this.props.data.settings.price}
            click={this.tabClick.bind(this)}
            title={prices.regular}
            name="regular"
          />
          {this.renderPer('regular', 'late')}
          <PriceTab
            current={this.props.data.settings.price}
            click={this.tabClick.bind(this)}
            title={prices.late}
            name="late"
          />
          {this.renderPer('late', 'onsite')}
          <PriceTab
            current={this.props.data.settings.price}
            click={this.tabClick.bind(this)}
            title={prices.onsite}
            name="onsite"
          />
        </ul>

        <div className="price-info" key={infokey}>
          {this.renderPriceinfo()}
        </div>
      </div>
    );
  }
}


function mapStateToProps({ data }){
  return { data };
}

export default connect(mapStateToProps, { changePrice })(PriceTabs);
