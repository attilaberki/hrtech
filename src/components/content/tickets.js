import React, { Component } from 'react';
import { connect } from 'react-redux';

import { changeCompare } from '../../actions/index';
import Ticket from "./ticket";

class Tickets extends Component {

  state = {
    phase: 'first',
    seleced: null,
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ phase: 'rendered' });
  }

  clickCompare() {
    this.props.changeCompare(!this.props.data.compare)
  }

  selectTicket(id) {
    this.setState({ selected: id });
  }

  resetTicket(id) {
    this.setState({ selected: null });
  }

  renderTickets() {

    const selected = this.props.data.pages.filter((page) => {
      return page.name === this.props.data.settings.page;
    });

    return selected[0].tickets.map((ticket, k) => {
      const key = `${selected[0].name}-${k}`;
      return (
        <Ticket
          data={ticket}
          price={this.props.data.settings.price}
          labels={this.props.data.labels}
          button="Book now"
          compare={this.props.data.compare}
          id={key}
          key={key}
          selected={(key === this.state.selected)}
          click={this.selectTicket.bind(this)}
          reset={this.resetTicket.bind(this)}
        />
      )
    })

    return null;
  }

  render() {
    const classname = `Tickets ${this.state.phase}`;
    const compareclass = this.props.data.compare ? "compare opened" : "compare closed";
    return (
      <div className={classname}>
        <ul className="ticketul">
          {this.renderTickets()}
        </ul>

        <a className={compareclass} onClick={this.clickCompare.bind(this)}>
          <span className="closed">{this.props.data.labels.compare.closed}</span>
          <span className="opened">{this.props.data.labels.compare.opened}</span>
        </a>
      </div>
    );
  }
}


function mapStateToProps({ data }){
  return { data };
}

export default connect(mapStateToProps, { changeCompare })(Tickets);
