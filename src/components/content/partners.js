import React, { Component } from 'react';
import { connect } from 'react-redux';

class Partners extends Component {

  renderPartners() {
    return this.props.data.partners.logos.map((logo, k) => {
      const key = `partner-${k}`;
      const src = `assets/partners/${logo.icon}`;
      return (
        <a href={logo.url} target="_blank" key={key}>
          <img src={src} alt="" />
        </a>
      )
    })
  }

  render() {
    return (
      <div className="Partners">
        <h5>{this.props.data.partners.label}</h5>
        <div className="partner-links">
          {this.renderPartners()}
        </div>
      </div>
    );
  }
}


function mapStateToProps({ data }){
  return { data };
}

export default connect(mapStateToProps)(Partners);
