import React, { Component } from 'react';

class PriceTab extends Component {

  className() {
    if (this.props.name === this.props.current) {
      return `tab active ${this.props.name}`;
    }
    return `tab ${this.props.name}`;
  }

  click() {
    this.props.click(this.props.name);
  }

  renderSoldout() {
    if (this.props.soldout) {
      return <b className="tag">{this.props.soldout}</b>;
    }
    return null;
  }

  renderClock() {
    if (this.props.until) {
      return (
        <b className="clock">
          <i className="fa fa-clock-o"></i>
          <p className="until">{this.props.until}</p>
          <br/>
          <p className="date">{this.props.date}</p>
        </b>
      );
    }
    return null;
  }

  render() {
    return (
      <li className={this.className()} onClick={this.click.bind(this)}>
        <span>{this.props.title}</span>
        {this.renderSoldout()}
        {this.renderClock()}
      </li>
    );
  }
}


export default PriceTab;
