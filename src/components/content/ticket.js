import React, { Component } from 'react';

class Ticket extends Component {

  state = {
    inputfocus: false,
    inputvalue: "",
    selecteddate: null,
  }

  onClick() {
    this.props.click(this.props.id)
  }

  onFocus() {
    this.setState({ inputfocus: true });
  }

  onBlur() {
    this.setState({ inputfocus: false });
  }

  onChangeText(event) {
    this.setState({ inputvalue: event.target.value });
  }

  dateClick(type) {
    this.setState({ selecteddate: type });
  }

  placeholderClass() {
    if((this.state.inputvalue.length > 0) || this.state.inputfocus){
      return "placeholder hidden";
    }
    return "placeholder";
  }

  renderPrice() {
    return this.props.data.prices[this.props.price].price;
  }

  renderDetails() {
    return this.props.data.details.map((det, k) => {
      return <li key={k}>{det}</li>
    })
  }

  renderHighlight() {
    if (this.props.data.type === "highlighted") {
      return <div className="highlight">{this.props.data.highlight}</div>
    }
    return null;
  }

  renderSublines() {
    const untilkey = `${this.props.price}-untilkey`;
    if (this.props.data.type === "special") {
      return (
        <div className="special">
          <div className="label">{this.props.data.label}</div>
          <div className="special-inputcont">
            <span className={this.placeholderClass()}>
              {this.props.data.placeholder}
              <b>{this.props.data.daylabel}</b>
            </span>
            <input
              type="text"
              onFocus={this.onFocus.bind(this)}
              onBlur={this.onBlur.bind(this)}
              value={this.state.inputvalue}
              onChange={this.onChangeText.bind(this)}
            />
          </div>
        </div>
      );
    }
    return (
      <div>
        <div className="save">{this.props.data.save}</div>
        <div className="until" key={untilkey}>{this.props.data.prices[this.props.price].until}</div>
      </div>
    );
  }

  renderSelection(type) {
    if (type === this.state.selecteddate) {
      return "selected"
    }
    return "";
  }

  selectorClass() {
    if((this.state.inputvalue.length > 0) || this.state.inputfocus || (this.state.selecteddate !== null)){
      return "dateSelector opened";
    }
    return "dateSelector";
  }

  renderDateselector() {
    if (this.props.data.type === "special") {
      return (
        <div className={this.selectorClass()}>
          <span className={this.renderSelection(1)} onClick={this.dateClick.bind(this, 1)}>
            {this.props.labels.days.a.name}<b>{this.props.labels.days.a.date}</b>
          </span>
          <span className={this.renderSelection(2)} onClick={this.dateClick.bind(this, 2)}>
            {this.props.labels.days.b.name}<b>{this.props.labels.days.b.date}</b>
          </span>
          <span className={this.renderSelection('both')} onClick={this.dateClick.bind(this, 'both')}>
            {this.props.labels.days.c.name}<b>{this.props.labels.days.c.date}</b>
          </span>
        </div>
      );
    }

    return null;
  }


  render() {
    const pricekey = `${this.props.price}-pricekey`;
    const frontclass = `front ${this.props.price}`;
    const liclass = `ticketli ${this.props.data.type} ${this.props.selected}`;
    const detailsclass = this.props.compare ? "details opened" : "details"
    return (
      <li className={liclass}>
        <div className={frontclass}>
            <div className="datas">
              {this.renderHighlight()}
              <h2>{this.props.data.title}</h2>
              <div className="price" data-key={pricekey} key={pricekey}>
                {this.props.data.prices[this.props.price].price}
              </div>
              {this.renderSublines()}
            </div>

            {this.renderDateselector()}

            <ul className={detailsclass}>
              {this.renderDetails()}
            </ul>

            <a className="button" onClick={this.onClick.bind(this)}>{this.props.button}</a>
        </div>
        <div className="back">
            <div className="data">
              <h3>{this.props.labels.cardback.title}</h3>
              <i className="fa fa-ticket"></i>
              <div className="lead">{this.props.labels.cardback.lead}</div>
              <a className="goback" onClick={this.props.reset}>{this.props.labels.cardback.back}</a>
            </div>
        </div>
      </li>
    );
  }
}


export default Ticket;
