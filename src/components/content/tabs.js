import React, { Component } from 'react';
import { connect } from 'react-redux';

import { changePage, fetchData } from '../../actions/index';
import Tab from "./tab";

class Tabs extends Component {

  tabClick(name) {
    this.props.changePage(name);
  }

  renderTabs() {
    return this.props.data.pages.map((tab) => {
      return (
        <Tab
          title={tab.title}
          icon={tab.icon}
          name={tab.name}
          key={tab.name}
          current={this.props.data.settings.page}
          click={this.tabClick.bind(this)}
        />
      )
    })
  }

  render() {
    return (
      <div className="Tabs">
        <ul>
          {this.renderTabs()}
        </ul>
      </div>
    );
  }
}


function mapStateToProps({ data }){
  return { data };
}

export default connect(mapStateToProps, { changePage, fetchData })(Tabs);
