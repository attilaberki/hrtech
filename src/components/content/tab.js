import React, { Component } from 'react';

class Tab extends Component {

  className() {
    if (this.props.name === this.props.current) {
      return `tab active ${this.props.name}`;
    }
    return `tab ${this.props.name}`;
  }

  click() {
    this.props.click(this.props.name);
  }

  render() {
    const iconclass = `fa fa-${this.props.icon}`;
    return (
      <li className={this.className()} onClick={this.click.bind(this)}>
        <span><i className={iconclass}></i> {this.props.title}</span>
      </li>
    );
  }
}


export default Tab;
