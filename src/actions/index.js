import axios from 'axios';

const API_URL = 'https://attilaberki.bitbucket.io/hrn/api/test-datas.json';

export const FETCH_DATA = 'FETCH_DATA';
export const CHANGE_PAGE = 'CHANGE_PAGE';
export const CHANGE_PRICE = 'CHANGE_PRICE';
export const CHANGE_COMPARE = 'CHANGE_COMPARE';

export function fetchData(location) {
  const url = `${API_URL}`;
  const request = axios.get(url);

  return ({
    type: FETCH_DATA,
    payload: request
  });
}

export function changePage(name) {
  return ({
    type: CHANGE_PAGE,
    payload: name
  });
}

export function changePrice(name) {
  return ({
    type: CHANGE_PRICE,
    payload: name
  });
}

export function changeCompare(bool) {
  return ({
    type: CHANGE_COMPARE,
    payload: bool
  });
}
