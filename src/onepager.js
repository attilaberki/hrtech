import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchData, changePage } from './actions/index';

import Header from './components/header';
import Footer from './components/footer';
import Content from './components/content';

class Onepager extends Component {

  componentWillMount() {
    this.props.fetchData('amsterdam');
  }

  renderContent() {
    if (this.props.data.info !== null) {
      return (
        <div className="appWrapper">
          <Header />
          <Content />
          <Footer />
        </div>
      );
    } else {
      return null;
    }
  }

  render() {
    return (
      this.renderContent()
    );
  }
}


function mapStateToProps({ data }){
  return { data };
}

export default connect(mapStateToProps, { fetchData, changePage })(Onepager);
