
import { FETCH_DATA, CHANGE_PAGE, CHANGE_PRICE, CHANGE_COMPARE } from '../actions/index';

const INITIAL_STATE = {
  info: null,
  labels: null,
  pages: null,
  partners: null,
  settings: null,
  compare: false,
};

export default function(state = INITIAL_STATE, action) {
  switch(action.type) {
    case FETCH_DATA:
      return {
        ...state,
        info: action.payload.data.info,
        labels: action.payload.data.labels,
        pages: action.payload.data.pages,
        partners: action.payload.data.partners,
        settings: action.payload.data.settings,
      }
    case CHANGE_PAGE:
      return { ...state, settings: { page: action.payload, price: state.settings.price } }
    case CHANGE_PRICE:
      return { ...state, settings: { page: state.settings.page, price: action.payload } }
    case CHANGE_COMPARE:
      return { ...state, compare: action.payload }
    default:
      return state;
  }
}
