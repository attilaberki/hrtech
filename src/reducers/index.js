import { combineReducers } from 'redux';
import appReducer from './reducer-app';

const rootReducer = combineReducers({
  data: appReducer
});

export default rootReducer;
